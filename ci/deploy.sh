#!/usr/bin/env bash
set -x

docker rm backend -f

docker run -d -p 8009:8000 --name backend hello